package config

import (
	"encoding/json"
	"os"
)

type Configuration struct {
	DBpath string `json:"DBpath"`
}

func Configure(file string) (Configuration, error) {
	var cfg Configuration
	f, err := os.ReadFile(file)
	if err != nil {
		return cfg, err
	}
	json.Unmarshal(f, &cfg)

	return cfg, nil
}

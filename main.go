package main

import (
	"fmt"
	"os"
	"taskscout/config"
	"taskscout/db"
)

var cfgF string = "config.json" //convert to full path var

func init() {
	cfg, err := config.Configure(cfgF)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	db.Seed(cfg.DBpath)
}

func main() {
	cfg, err := config.Configure(cfgF)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	db.CreateTask("test", cfg.DBpath)
	db.TaskLog(cfg.DBpath, "test")
}

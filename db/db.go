package db

import (
	"database/sql"
	"fmt"
	"time"

	"github.com/google/uuid"
	_ "github.com/mattn/go-sqlite3"
)

//func (*db.cfg) client() {}

func Seed(DBpath string) error {
	db, err := sql.Open("sqlite3", DBpath)
	if err != nil {
		return err
	}
	defer db.Close()
	fmt.Printf("Seeding initial database at %s\n", DBpath)
	const createT1 string = `
	CREATE TABLE IF NOT EXISTS tasks(
	id INTIGER NOT NULL PRIMARY KEY,
	name TEXT,
	createdAT INTIGER
	);`

	const createT2 string = `
	CREATE TABLE IF NOT EXISTS log(
		time INTIGER PRIMARY KEY,
		task TEXT
	);`

	fmt.Printf("Creating table [tasks]...")
	db.Exec(createT1)
	fmt.Printf("Done!\n")
	fmt.Printf("Creating table [logs]...")
	db.Exec(createT2)
	fmt.Printf("Done!\n")
	return nil
}

func CreateTask(name string, DBpath string) error {
	fmt.Printf("Creating task [%s]...", name)
	id := uuid.New()
	t := time.Now().Unix()
	db, err := sql.Open("sqlite3", DBpath)
	if err != nil {
		return err
	}
	//var check string = ''
	db.Exec("INSERT INTO tasks VALUES(?,?,?);", id, name, t)
	defer db.Close()
	fmt.Printf("Done!\n")
	return nil

}

func TaskLog(DBpath string, task string) error {

	t := time.Now().Unix()
	db, err := sql.Open("sqlite3", DBpath)
	if err != nil {
		return err
	}
	fmt.Printf("Logging task [%s] at %s\n", task, time.Now().Format(time.Kitchen))
	db.Exec("INSERT INTO log VALUES(?,?)", t, task)
	defer db.Close()
	return nil
}

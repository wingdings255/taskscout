TARGET	?=	taskscout

all: build-frontend build-backend

test: test-frontend test-backend

test-frontend:
	cd web/ && npm run lint
	cd web/ && npm run test:unit
	cd web/ && npm run test:e2e
test-backend:
	go install gotest.tools/gotestsum@latest
	gotestsum --format testname
build-frontend:
	cd web/ && npm install
	cd web/ && npm run build
build-backend:
	go build -o $(TARGET)

docker:
	@docker build . -t $(TARGET):latest
